import { useAuth } from "react-oidc-context";
import { WeatherForecast } from "../types/WeatherForecast";

export function useApi() {
  const auth = useAuth();
  const get = (): Promise<WeatherForecast[]> => {
    const url = "http://localhost:5000/weatherforecast";
    const options = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${auth.user?.access_token}`,
        "Content-Type": "application/json",
      },
    };
    const request = async (): Promise<WeatherForecast[]> => {
      const resp = await fetch(url, options);
      const response = (await resp.json()) as WeatherForecast[];
      return response;
    };
    return request();
  };

  return [get];
}
