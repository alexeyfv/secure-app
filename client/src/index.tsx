import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "bootstrap/dist/css/bootstrap.min.css";
import { AuthProvider } from "react-oidc-context";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <AuthProvider
      authority={"http://localhost:8080/realms/secure-app"}
      client_id={"account"}
      redirect_uri={window.location.origin}
      scope="openid profile"
      automaticSilentRenew={true}
      loadUserInfo={true}
    >
      <App />
    </AuthProvider>
  </React.StrictMode>
);
