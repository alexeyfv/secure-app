import WeatherForecastView from "./WeatherForecastView";
import { useAuth } from "react-oidc-context";

export default function App() {
  const auth = useAuth();

  return (
    <div className="container mt-5 mb-5">
      {auth.isAuthenticated ? (
        <div className="App">
          <WeatherForecastView />
        </div>
      ) : (
        <div className="row">
          <div className="col text-center">
            {auth.isLoading ? (
              <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            ) : auth.error ? (
              <div className="alert alert-danger" role="alert">
                Error
              </div>
            ) : (
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => void auth.signinRedirect()}
              >
                Log in
              </button>
            )}
          </div>
        </div>
      )}
    </div>
  );
}
