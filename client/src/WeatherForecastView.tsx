import { useEffect, useState } from "react";
import { useApi } from "./hooks/useApi";
import { WeatherForecast } from "./types/WeatherForecast";

export default function WeatherForecastView() {
  const [get] = useApi();
  const [forecast, setForecast] = useState<WeatherForecast[]>([]);

  useEffect(() => {
    get().then((wf) => setForecast(wf));
  }, []);

  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">Date</th>
          <th scope="col">Temperature</th>
          <th scope="col">Summary</th>
        </tr>
      </thead>
      <tbody>
        {forecast.map((f) => (
          <tr>
            <td>{new Date(f.date).toDateString()}</td>
            <td>{f.temperatureC}</td>
            <td>{f.summary}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
