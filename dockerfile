# Stage 1: Frontend - React SPA (https://hub.docker.com/_/node)
FROM node:18-alpine AS frontend-build
WORKDIR /
COPY ./client ./
RUN npm i && npm run build

# Stage 2: Backend - ASP.NET Core Web API (https://hub.docker.com/_/microsoft-dotnet-sdk/)
FROM mcr.microsoft.com/dotnet/sdk:7.0-alpine AS backend-build
WORKDIR /

# Copy projects and restore dependencies
COPY ./server/*.csproj ./server/
RUN dotnet restore ./server/

# Copy everything else
COPY . .

# Build app
WORKDIR /server
RUN dotnet publish -c release -o /app

# Stage 3: Create final container
FROM mcr.microsoft.com/dotnet/aspnet:7.0-alpine
WORKDIR /app
COPY --from=frontend-build /build ./wwwroot
COPY --from=backend-build /app ./
ENTRYPOINT ["dotnet", "server.dll"]
